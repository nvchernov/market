﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using ActiveSupport;
using Market.Infrastructure.Services;
using Market.ViewModels;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;


namespace Market.Controllers
{
    public class SalesOrderController : Controller
    {
        private ISalesOrderService m_SalesOrderService;

        public SalesOrderController(ISalesOrderService salesOrderService)
        {
            m_SalesOrderService = salesOrderService;
        }

        // GET: SalesOrder
        public ActionResult Index(int? page = null)
        {
            SalesOrderListViewModel salesOrderListViewModel = new SalesOrderListViewModel();
            salesOrderListViewModel.Count = m_SalesOrderService.SalesOrdresCount();

            if (page.IsPresent())
                salesOrderListViewModel.SalesOrders = m_SalesOrderService.GetPage(page.Value).ToList();
            else
                salesOrderListViewModel.SalesOrders = m_SalesOrderService.GetPage(0).ToList();

            return View(salesOrderListViewModel);
        }

        public IActionResult Page(int id)
        {
            SalesOrderListViewModel salesOrderListViewModel = new SalesOrderListViewModel();
            salesOrderListViewModel.Count = m_SalesOrderService.SalesOrdresCount();
            salesOrderListViewModel.SalesOrders = m_SalesOrderService.GetPage(id).ToList();

            return Json(salesOrderListViewModel);
        }

        public IActionResult Show(int id)
        {
            return View(m_SalesOrderService.GetSalesOrderVM(id));
        }

        public ActionResult Create()
        {
            return View(m_SalesOrderService.GetSalesOrderVM());
        }

        // POST: SalesOrder/Create
        [HttpPost]
        public ActionResult Create([FromBody]SalesOrderViewModel salesOrderViewModel)
        {
            var errorList = ModelState.Values
                .SelectMany(m => m.Errors)
                .Select(e => e.ErrorMessage)
                .ToList();

            if (errorList.IsPresent())
            {
                HttpContext.Response.StatusCode = 418;
                return Json(errorList);
            }
            else
            {
                var vm = m_SalesOrderService.CreateOrUpdate(salesOrderViewModel);
                return Json(m_SalesOrderService.GetSalesOrderVM(vm.Id));
            }

        }

        // GET: SalesOrder/Edit/5
        public ActionResult Edit(int id)
        {
            return View("~/Views/SalesOrder/Create.cshtml", m_SalesOrderService.GetSalesOrderVM(id));
        }




    }
}