﻿using AutoMapper;
using Market.Models;
using Market.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Market
{
    public class Automapper
    {
        private static Mapper m_Mapper;
        public static Mapper Mapper { get => m_Mapper; }

        public static void Init()
        {
                
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<SalesOrder, SalesOrderViewModel>();
                cfg.CreateMap<SalesOrderViewModel, SalesOrder>();

                cfg.CreateMap<SalesOrderDetail, SalesOrderDetailViewModel>();
                cfg.CreateMap<SalesOrderDetailViewModel, SalesOrderDetail>();
            });
            

        }


    }
}
