﻿using Market.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Dapper.Contrib;
using Market.Models;
using AutoMapper;
using Dapper.Contrib.Extensions;
using ActiveSupport;

namespace Market.Infrastructure.Services
{
    public class SalesOrderService : ISalesOrderService
    {
        private IDbConnection m_DbConnection;
        public SalesOrderService(IDbConnection dbConnection)
        {
            m_DbConnection = dbConnection;
        }

        public SalesOrderViewModel GetSalesOrderVM()
        {
            var salesOrderVM = new SalesOrderViewModel();
            salesOrderVM.OrderDate = DateTime.Now;

            salesOrderVM.Statuses = m_DbConnection.Query<SalesStatus>("SELECT * FROM SalesStatus").ToList();
            salesOrderVM.Customers = m_DbConnection.Query<Customer>("SELECT * FROM Customer").ToList();
            salesOrderVM.Products = m_DbConnection.Query<Product>("SELECT * FROM Product").ToList();

            return salesOrderVM;
        }

        public SalesOrderViewModel GetSalesOrderVM(long id)
        {
            var salesOrder = m_DbConnection.QueryFirstOrDefault<SalesOrder>("SELECT * FROM SalesOrder WHERE id = @id", new { id = id });

            if (salesOrder == null)
                return null;

            var salesOrderViewModel = new SalesOrderViewModel();
            salesOrderViewModel.Statuses = m_DbConnection.Query<SalesStatus>("SELECT * FROM SalesStatus").ToList();
            salesOrderViewModel.Customers = m_DbConnection.Query<Customer>("SELECT * FROM Customer").ToList();
            salesOrderViewModel.Products = m_DbConnection.Query<Product>("SELECT * FROM Product").ToList();

            Mapper.Map(salesOrder, salesOrderViewModel);

            salesOrderViewModel.SalesOrdersDetails = m_DbConnection.Query<SalesOrderDetail, Product, SalesOrderDetailViewModel>(
                @"SELECT * FROM SalesOrderDetail JOIN Product ON SalesOrderDetail.ProductId = Product.Id WHERE SalesOrderDetail.SalesOrderId = @id",
                (salesOrderDetail, product) =>
                {
                    var salesOrderDetailViewModel = new SalesOrderDetailViewModel()
                    {
                        Product = product
                    };

                    Mapper.Map(salesOrderDetail, salesOrderDetailViewModel);

                    return salesOrderDetailViewModel;

                }, new { id = salesOrderViewModel.Id }).ToList();

            return salesOrderViewModel;

        }

        public SalesOrderViewModel CreateOrUpdate(SalesOrderViewModel salesOrderViewModel)
        {
            if (salesOrderViewModel.Id == 0)
            {
                SalesOrder salesOrder = new SalesOrder();
                var salesOrderDetails = salesOrderViewModel.SalesOrdersDetails?.Select(x =>
                {
                    SalesOrderDetail salesOrderDetail = new SalesOrderDetail();
                    Mapper.Map(x, salesOrderDetail);

                    return salesOrderDetail;
                });

                Mapper.Map(salesOrderViewModel, salesOrder);
                m_DbConnection.Insert(salesOrder);

                salesOrderViewModel.Id = salesOrder.Id;
                salesOrderViewModel.SalesOrdersDetails.ForEach(x => x.SalesOrderId = salesOrder.Id);
                salesOrderViewModel.SalesOrdersDetails.ForEach(x => x.UnitPrice = x.Product.ListPrice);

                m_DbConnection.Insert(salesOrderDetails);
            }
            else
            {
                var salesOrder = m_DbConnection.Get<SalesOrder>(salesOrderViewModel.Id);

                var orderDetailsIds = m_DbConnection.Query<long>(
                    "SELECT Id FROM SalesOrderDetail WHERE SalesOrderId = @id",
                    new { id = salesOrder.Id });

                // ------ сохраняем продукты ордера ------

                salesOrderViewModel.SalesOrdersDetails.ForEach(x => x.SalesOrderId = salesOrder.Id);
                salesOrderViewModel.SalesOrdersDetails.ForEach(x => x.UnitPrice = x.Product.ListPrice);

                var salesOrderDetails = salesOrderViewModel.SalesOrdersDetails
                    .Select(x =>
                    {
                        SalesOrderDetail salesOrderDetail = new SalesOrderDetail();
                        Mapper.Map(x, salesOrderDetail);

                        return salesOrderDetail;
                    });

                var newSalesOrderDetails = salesOrderDetails.Where(x => x.Id == 0);
                var storedSalesOrderDetails = salesOrderDetails.Where(x => x.Id > 0);

                if (newSalesOrderDetails.IsPresent())
                    m_DbConnection.Insert(newSalesOrderDetails);

                if (storedSalesOrderDetails.IsPresent())
                    m_DbConnection.Update(storedSalesOrderDetails);

                // ------ удаляем SalesOrderDetails, который удалил юзер в форме ------

                var removedOrderDetails = orderDetailsIds.Except(storedSalesOrderDetails.Select(x => x.Id));

                if (removedOrderDetails.IsPresent())
                    m_DbConnection.Execute("DELETE FROM SalesOrderDetail WHERE Id IN @ids", new { ids = removedOrderDetails.ToArray() });

                // ------ сохраняем ордер ------

                Mapper.Map(salesOrderViewModel, salesOrder);

                m_DbConnection.Update(salesOrder);
            }

            return salesOrderViewModel;
        }

        public IEnumerable<SalesOrderListItem> GetPage(int page)
        {
            return
                m_DbConnection.Query<SalesOrder, SalesStatus, Customer, SalesOrderListItem>(
                @"SELECT * FROM SalesOrder 
                JOIN SalesStatus ON SalesOrder.StatusId = SalesStatus.Id 
                JOIN Customer ON SalesOrder.CustomerId = Customer.Id 
                ORDER BY Id desc
                LIMIT @page, 10",
                (salesOrder, salesStatus, customer) =>
                {
                    var res = new SalesOrderListItem()
                    {
                        Comment = salesOrder.Comment,
                        OrderDate = salesOrder.OrderDate,
                        Customer = customer,
                        Status = salesStatus,
                        Id = salesOrder.Id
                    };

                    return res;
                },
                new { page = page * 10 }
                );
        }

        public long SalesOrdresCount() =>
            m_DbConnection.ExecuteScalar<long>("SELECT COUNT(*) FROM SalesOrder");

    }

}
