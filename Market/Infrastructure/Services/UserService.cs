﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Market.Models;
using Market.Infrastructure.Repository;

namespace Market.Infrastructure.Services
{
    public class UserService : IUserService
    {
        public const string ROLE_CLAIM = "Role";
        public const string ID_CLAIM = "Id";

        private HttpContext m_HttpContext;
        private IRepositoryProvider m_RepositoryProvider;

        public UserService(IHttpContextAccessor httpContextAccessor, IRepositoryProvider repositoryProvider)
        {
            m_HttpContext = httpContextAccessor.HttpContext;
            m_RepositoryProvider = repositoryProvider;
        }

        private User currentUser = null;
        public User CurrentUser => 
            currentUser = currentUser ?? m_RepositoryProvider.UserRepository.GetByEmail(m_HttpContext.User.Identity.Name);

        #region claim features

        public bool IsCurrentUserAnon => !(bool)m_HttpContext?.User?.Identity.IsAuthenticated;

        public bool IsCurrentUserAuthenticated => (bool)m_HttpContext?.User?.Identity.IsAuthenticated;

        public string CurrentUserName => m_HttpContext?.User?.Claims.First(x => x.Type == ClaimsIdentity.DefaultNameClaimType).Value;

        public string CurrentUserRole => m_HttpContext?.User?.Claims.First(x => x.Type == ROLE_CLAIM).Value;

        public int CurrentUserId => Int32.Parse(m_HttpContext?.User?.Claims.First(x => x.Type == ID_CLAIM).Value);
        
        #endregion

        #region auth

        public User Login(string email, string password)
        {
            var user = m_RepositoryProvider.UserRepository.GetByEmailHash(email, HashString(password));

            if (user != null)
                Authenticate(user);
            
            return user;
        }

        public User Register(string email, string password, string name)
        {
            var user = m_RepositoryProvider.UserRepository.GetByEmail(email);

            if (user == null)
            {
                m_RepositoryProvider.UserRepository.Create(user = new User
                {
                    Email = email,
                    Password = password,
                    Hash = HashString(password),
                    Name = name,
                    Role = UserRole.User
                });

                Authenticate(user);
            }
            else
            {
                return null;
            }

            return user;
        }

        public string HashString(string str)
        {
            if (string.IsNullOrWhiteSpace(str))
                return null;

            using (var algorithm = SHA256.Create())
            {
                var hash = algorithm.ComputeHash(Encoding.ASCII.GetBytes(str));
                return Encoding.ASCII.GetString(hash);
            }
        }

        private void Authenticate(User user)
        {
            // создаем один claim
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Email),
                new Claim(ID_CLAIM, user.Id.ToString()),
                new Claim(ROLE_CLAIM, user.Role.ToString()),
            };

            // создаем объект ClaimsIdentity
            ClaimsIdentity identity = new ClaimsIdentity(
                claims, 
                "ApplicationCookie",
                ClaimsIdentity.DefaultNameClaimType,
                ClaimsIdentity.DefaultRoleClaimType);

            // установка аутентификационных куки
            m_HttpContext.SignInAsync("Cookies", new ClaimsPrincipal(identity));

        }
        
        #endregion

    }
}
