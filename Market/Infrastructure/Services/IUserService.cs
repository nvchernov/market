﻿using Market.Models;

namespace Market.Infrastructure.Services
{
    public interface IUserService
    {
        User CurrentUser { get; }
        int CurrentUserId { get; }
        string CurrentUserName { get; }
        string CurrentUserRole { get; }
        bool IsCurrentUserAnon { get; }
        bool IsCurrentUserAuthenticated { get; }

        string HashString(string str);
        User Login(string email, string password);
        User Register(string email, string password, string name);
    }
}