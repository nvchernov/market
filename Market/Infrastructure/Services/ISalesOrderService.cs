﻿using Market.ViewModels;
using System.Collections.Generic;

namespace Market.Infrastructure.Services
{
    public interface ISalesOrderService
    {
        SalesOrderViewModel GetSalesOrderVM(long id);
        SalesOrderViewModel GetSalesOrderVM();

        SalesOrderViewModel CreateOrUpdate(SalesOrderViewModel salesOrderViewModel);

        IEnumerable<SalesOrderListItem> GetPage(int page);

        long SalesOrdresCount();
    }
}