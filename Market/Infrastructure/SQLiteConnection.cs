﻿using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Market.Infrastructure
{
    public class SQLiteConnection : IDisposable
    {
        private SqliteConnection m_Connection;
        public SqliteConnection Connection { get => m_Connection; }
        public object Users { get; internal set; }

        public SQLiteConnection()
        {
            m_Connection = new SqliteConnection($"Data Source=db.db");
            m_Connection.Open();
        }

        ~SQLiteConnection()
        {
            Dispose(false);
        }

        protected bool m_Disposed = false;

        protected virtual void Dispose(bool disposeManaged)
        {
            if (m_Disposed)
                return;

            if (disposeManaged)
                m_Connection?.Dispose();

            m_Disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);

        }
    }
}
