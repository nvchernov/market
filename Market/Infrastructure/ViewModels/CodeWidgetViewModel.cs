﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Market.Infrastructure.ViewModels
{
    public class CodeWidgetViewModel
    {



        private bool m_AutoHeight;
        public bool AutoHeight { get => m_AutoHeight; set => m_AutoHeight = value; }

        private string m_Code;
        public string Code { get => m_Code; set => m_Code = value; }


        private bool m_ReadOnly;
        public bool ReadOnly { get => m_ReadOnly; set => m_ReadOnly = value; }


        private bool m_PreparedCode;
        public bool ShowPreparedCode { get => m_PreparedCode; set => m_PreparedCode = value; }


        private bool m_ShowPlayButton;
        public bool ShowPlayButton { get => m_ShowPlayButton; set => m_ShowPlayButton = value; }
    }
}
