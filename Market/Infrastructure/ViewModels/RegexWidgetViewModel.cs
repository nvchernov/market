﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Market.Infrastructure.ViewModels
{
    public class RegexWidgetViewModel
    {

        private string m_HtmlId = "editor";
        public string HtmlId { get => m_HtmlId; set => m_HtmlId = value; }

        private bool m_IsReadOnly;
        public bool IsReadOnly { get => m_IsReadOnly; set => m_IsReadOnly = value; }

        private string m_Mask;
        public string Mask { get => m_Mask; set => m_Mask = value; }

        private string m_Text;
        public string Text { get => m_Text; set => m_Text = value; }

        private string m_Data;
        /// <summary>
        /// Json data to render (RegexResult)
        /// </summary>
        public string Data { get => m_Data; set => m_Data = value; }

        private bool m_ReplaceMode;
        public bool ReplaceMode { get => m_ReplaceMode; set => m_ReplaceMode = value; }

        private bool m_HideStatistics;
        public bool HideStatistics { get => m_HideStatistics; set => m_HideStatistics = value; }

    }
}
