﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Market.Infrastructure
{
    public class HTMLMinifierMiddleware
    {
        private readonly RequestDelegate m_Next;

        public HTMLMinifierMiddleware(RequestDelegate next)
        {
            m_Next = next;
        }

        private Regex m_SpaceBetweenTagRegExp = new Regex(@">\s+<", RegexOptions.Compiled);

        public async Task InvokeAsync(HttpContext context)
        {

            var stream = context.Response.Body;
            using (var buffer = new MemoryStream())
            {
                context.Response.Body = buffer;
                await m_Next(context);

                buffer.Seek(0, SeekOrigin.Begin);
                using (var reader = new StreamReader(buffer))
                {
                    string responseBody = await reader.ReadToEndAsync();
                    var isHtml = context.Response.ContentType?.ToLower().Contains("text/html");
                    if (context.Response.StatusCode == 200 && isHtml.GetValueOrDefault())
                    {
                        responseBody = m_SpaceBetweenTagRegExp.Replace(responseBody, "><");

                        using (var memoryStream = new MemoryStream())
                        {
                            var bytes = Encoding.UTF8.GetBytes(responseBody);
                            memoryStream.Write(bytes, 0, bytes.Length);
                            memoryStream.Seek(0, SeekOrigin.Begin);
                            await memoryStream.CopyToAsync(stream, bytes.Length);
                        }
                    }
                }
            }



        }
    }
}