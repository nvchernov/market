﻿using Microsoft.AspNetCore.Html;
using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Encodings.Web;
using System.Threading.Tasks;
using Market.Infrastructure.ViewModels;

namespace Market.Infrastructure
{
    public static class HtmlHelperExtentions
    {
        public static async Task<IHtmlContent> RenderCodeWidget(this IHtmlHelper htmlHelper, string code = null, bool readOnly = false, bool includePreparedCode = false, bool showPlayButton = true, bool autoHeight = false)
        {
            return await htmlHelper.PartialAsync("~/Views/Code/_CodeWidget.cshtml", new CodeWidgetViewModel()
            {
                Code = code,
                ReadOnly = readOnly,
                ShowPreparedCode = includePreparedCode,
                ShowPlayButton = showPlayButton,
                AutoHeight = autoHeight
            });
        }

        public static async Task<IHtmlContent> RenderCodeReadonlyWidget(this IHtmlHelper htmlHelper, string code)
        {
            return await htmlHelper.PartialAsync("~/Views/Code/_CodeWidget.cshtml", new CodeWidgetViewModel()
            {
                Code = code,
                ReadOnly = true,
                ShowPreparedCode = false,
                ShowPlayButton = false,
                AutoHeight = true
            });

        }
        
        public static async Task<IHtmlContent> RenderRegexWidget(this IHtmlHelper htmlHelper, RegexWidgetViewModel regexWidgetViewModel)
        {
            return await htmlHelper.PartialAsync("~/Views/Code/_RegexWidget.cshtml", regexWidgetViewModel);
        }

        public static string GetString(this IHtmlContent content)
        {
            var writer = new System.IO.StringWriter();
            content.WriteTo(writer, HtmlEncoder.Default);
            return writer.ToString();
        }
    }
}
