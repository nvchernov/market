﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using Market.Models;
using Dapper;

namespace Market.Infrastructure.Repository
{
    public class UserRepository : BaseRepository<User>
    {
        public UserRepository(IDbConnection dbConnection) : base(dbConnection) { }

        protected override string TableName => "Users";

        public User GetByEmail(string email)
        {
            return Connection.QueryFirst($"SELECT * FROM {TableName} WHERE Email = @Email", new { Email = email });
        }

        public User GetByEmailHash(string email, string hash)
        {
            return Connection.QueryFirstOrDefault<User>(
                $"SELECT * FROM {TableName} WHERE Email = @Email AND Hash = @Hash",
                new { Email = email, Hash = hash });
        }

    }
}
