﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Market.Infrastructure.Repository
{
    public interface IRepositry<T>// where T: class
    {
        T GetById(long id);

        T Create(T entity);

        void Update(T entity);

        void Delete(T entity);
    }
}
