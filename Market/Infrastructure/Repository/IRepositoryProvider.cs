﻿namespace Market.Infrastructure.Repository
{
    public interface IRepositoryProvider
    {
        UserRepository UserRepository { get; set; }
    }
}