﻿using Dapper;
using Dapper.Contrib;
using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Threading.Tasks;
using Market.Models;

namespace Market.Infrastructure.Repository
{
    public abstract class BaseRepository<T> : IRepositry<T> where T : class, IEntity
    {
        protected abstract string TableName { get; }
        protected IDbConnection m_DbConnection;
        protected IDbConnection Connection => m_DbConnection;

        public BaseRepository(IDbConnection dbConnection)
        {
            m_DbConnection = dbConnection;
        }

        public T Create(T entity)
        {
            entity.Id = m_DbConnection.Insert(entity);
            
            return entity;
        }

        public void Delete(T entity)
        {
            m_DbConnection.Delete(entity);
        }

        public T GetById(long id)
        {
            return m_DbConnection.Get<T>(id);
        }

        public void Update(T entity)
        {
            m_DbConnection.Update(entity);
        }
    }
}
