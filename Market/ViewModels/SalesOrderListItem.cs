﻿using Market.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Market.ViewModels
{
    public class SalesOrderListItem
    {

        private long m_Id;
        public long Id { get => m_Id; set => m_Id = value; }

        private DateTime m_OrderDate;
        public DateTime OrderDate { get => m_OrderDate; set => m_OrderDate = value; }

        private SalesStatus m_Status;
        public SalesStatus Status { get => m_Status; set => m_Status = value; }

        private Customer m_Customer;
        public Customer Customer { get => m_Customer; set => m_Customer = value; }

        private string m_Comment;
        public string Comment { get => m_Comment; set => m_Comment = value; }

    }
}
