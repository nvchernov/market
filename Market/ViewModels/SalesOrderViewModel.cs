﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Market.Models;
using Market.ViewModels;

namespace Market.ViewModels
{
    public class SalesOrderViewModel : Entity
    {

        private DateTime m_OrderDate;
        public DateTime OrderDate { get => m_OrderDate; set => m_OrderDate = value; }

        private long m_StatusId;
        [Range(1, long.MaxValue, ErrorMessage = "Поле Статус не заполнено")]
        public long StatusId { get => m_StatusId; set => m_StatusId = value; }

        private List<SalesStatus> m_Statuses;
        public List<SalesStatus> Statuses { get => m_Statuses; set => m_Statuses = value; }


        private long m_CustomerId;
        [Range(1, long.MaxValue, ErrorMessage = "Поле Покуптель не заполнено")]
        public long CustomerId { get => m_CustomerId; set => m_CustomerId = value; }

        private List<Customer> m_Customers = new List<Customer>();        
        public List<Customer> Customers { get => m_Customers; set => m_Customers = value; }


        private List<Product> m_Products;
        public List<Product> Products { get => m_Products; set => m_Products = value; }

        private string m_Comment;
        [MaxLength(2000)]
        public string Comment { get => m_Comment; set => m_Comment = value; }

        private List<SalesOrderDetailViewModel> m_SalesOrdersDetails = new List<SalesOrderDetailViewModel>();
        [Required, MinLength(1, ErrorMessage = "Добавьте хотя бы один товар")]
        public List<SalesOrderDetailViewModel> SalesOrdersDetails
        {
            get => m_SalesOrdersDetails;
            set => m_SalesOrdersDetails = value;
        }

    }
}
