﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Market.ViewModels
{
    public class SalesOrderListViewModel
    {
        private List<SalesOrderListItem> m_SalesOrders;
        public List<SalesOrderListItem> SalesOrders { get => m_SalesOrders; set => m_SalesOrders = value; }

        private long m_Count;
        public long Count { get => m_Count; set => m_Count = value; }
    }
}