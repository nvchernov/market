﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Market.Models
{
    public class Product : Entity
    {

        private string m_Name;
        public string Name { get => m_Name; set => m_Name = value; }

        private double m_ListPrice;
        public double ListPrice { get => m_ListPrice; set => m_ListPrice = value; }

        private string m_Comment;
        public string Comment { get => m_Comment; set => m_Comment = value; }


    }
}
