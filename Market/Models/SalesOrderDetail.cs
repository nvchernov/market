﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Market.Models
{
    [Table("SalesOrderDetail")]
    public class SalesOrderDetail : Entity
    {

        private long m_SalesOrderId;
        public long SalesOrderId { get => m_SalesOrderId; set => m_SalesOrderId = value; }

        private long m_ProductId;
        public long ProductId { get => m_ProductId; set => m_ProductId = value; }

        private ulong m_OrderQty;
        public ulong OrderQty { get => m_OrderQty; set => m_OrderQty = value; }

        private double m_UnitPrice;
        public double UnitPrice { get => m_UnitPrice; set => m_UnitPrice = value; }

        private DateTime m_ModifyDate;
        public DateTime ModifyDate { get => m_ModifyDate; set => m_ModifyDate = value; }


    }
}
