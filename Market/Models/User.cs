﻿using Market.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Market.Models
{
    public class User : IEntity, IUser
    {

        private long m_Id;
        public long Id { get => m_Id; set => m_Id = value; }

        public string Name { get; set; }
        public string Email { get; set; }
        public string Hash { get; set; }
        public string Password { get; set; }

        public UserRole Role { get; set; }
    }

    public enum UserRole : int
    {
        User,


        Admin = 2_101_202
    }
}


