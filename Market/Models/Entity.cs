﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Market.Models
{
    public abstract class Entity : IEntity
    {
        private long m_Id;
        [Key]
        public long Id { get => m_Id; set => m_Id = value; }

    }
}
