﻿namespace Market.Models
{
    public interface IUser
    {
        string Email { get; set; }
        string Hash { get; set; }
        string Name { get; set; }
        string Password { get; set; }
        UserRole Role { get; set; }
    }
}