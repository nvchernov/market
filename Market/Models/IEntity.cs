﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Market.Models
{
    public interface IEntity
    {
        long Id { get; set; }
    }
}
