﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Market.Models
{
    [Table("SalesOrder")]
    public class SalesOrder : Entity
    {

        private DateTime m_OrderDate;
        public DateTime OrderDate { get => m_OrderDate; set => m_OrderDate = value; }

        private long m_StatusId;
        public long StatusId { get => m_StatusId; set => m_StatusId = value; }

        private long m_CustomerId;
        public long CustomerId { get => m_CustomerId; set => m_CustomerId = value; }


        private string m_Comment;
        public string Comment { get => m_Comment; set => m_Comment = value; }

    }
}
